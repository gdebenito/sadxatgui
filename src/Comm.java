/*
  File-Name: Comm.java
  Date: 2017 Apr 25
  Author: Gonzalo De Benito
  Purpose: Make default communications Data Used: none
  Data Output: none
*/
package src;
//import java.util.*;
//import java.io.*;
import java.lang.String;


public class Comm
{

    public static final String NICK       = "NICK";
    public static final String ACK        = "ACK";
    public static final String DISCONNECT = "DISC";
    public static final String NEWUSER    = "NEWUSR";
    public static final String DELUSER    = "DELUSR";
    public static final String NEWNICK    = "NEWNICK";
    public static final String PRIVATEMSG = "PRMSG";
    public static final String DELIMITER  = "=";
    
    public Comm()
    {

    }
}
