/* File-Name:   Server.java
   Date:        2017 Apr 18
   Author:      Gonzalo De Benito
   Purpose:     Start up a ServerSocker and accepts connections, 
                this are maintained by process thread.
   Data Used:   None
   Output File: Server.class
   Data Output: Console log
*/


package src;

import java.io.*;
//import java.lang.Thread;

public class Server {

    public static void main(String[] args) {
        try{
            MyServerSocket ss = new MyServerSocket(Integer.parseInt(args[0]));
	    Monitor m = new Monitor();
            while (true) {
                MySocket s = ss.accept();
		new Process(m,s).start();
	    }
	} catch (IndexOutOfBoundsException ex) {
	    System.out.println("Usage: <port> ex: 2222");
	} finally {
            //nothing here
	}
    }
}
