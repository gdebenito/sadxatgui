package src;

import java.awt.*;
import java.awt.event.*;
import java.awt.event.WindowAdapter;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.SwingWorker;
import java.util.List;
import java.util.HashMap; // for private xat's manager
import java.io.IOException;
import src.MySocket;
import src.Comm;

// todo: autoscroll JList
// use : nc -lp2222

/* ListDemo.java requires no other files. */
public class Xat extends JPanel implements ActionListener/*for buttons*/{

    // define a JList and a DefaultListModel
    private JList<String> list;
    private JList<String> userList;
    private static DefaultListModel<String> listModel;
    private DefaultListModel<String> userListModel;
    private JTextField entry;
    private JButton add;

    private static String ip;
    private static String nick;
    private static int port;
    private static MySocket ms;

    private static HashMap<String,PrivateXat> xatList = new HashMap<>();

    public Xat() {
        super(new BorderLayout(4,4));

        // create initial listModel
        listModel = new DefaultListModel<>();
        listModel.addElement("Welcome to the chat");

        userListModel = new DefaultListModel<>();
        userListModel.addElement("___Users___");

        //Create the list and put it in a scroll pane.
        list = new JList<>(listModel);
        JScrollPane listScrollPane = new JScrollPane(list);
        add(listScrollPane, BorderLayout.CENTER);

        userList = new JList<>(userListModel);
        JScrollPane userListScrollPane = new JScrollPane(userList);
        add(userListScrollPane, BorderLayout.LINE_END);

        JPanel inp = new JPanel();
        inp.setLayout(new BoxLayout(inp, BoxLayout.LINE_AXIS));
        add = new JButton("Send");
        entry = new JTextField();

        inp.add(entry);
        inp.add(add);

        entry.addActionListener(this);
        add.addActionListener(this);

        add(inp, BorderLayout.PAGE_END);

        new Thread() {

            @Override
            public void run() {
                String line;
                String rcvNick; 
                while ((line=ms.readLine())!=null) {
                    if (line.matches(Comm.NICK)) {
                        ms.println(nick);
                    } else if (line.matches(Comm.DISCONNECT)) {
                        break;
                    } else if (line.contains(Comm.NEWNICK)) {
                        nick = line.split(Comm.NEWNICK)[1];
                    } else if (line.contains(Comm.NEWUSER)) {
                        userListModel.addElement(
                                line.split(Comm.NEWUSER)[1]);
                    } else if (line.contains(Comm.DELUSER)) {
                        userListModel.removeElement(
                                line.split(Comm.DELUSER)[1]);
                    } else if (line.contains(Comm.PRIVATEMSG)) {
                        rcvNick = line.split(Comm.DELIMITER)[1];
                        if (!xatList.containsKey(rcvNick)) { 
                            createPrivateXat(rcvNick);
                        }
                        PrivateXat px = xatList.get(rcvNick);
                        px.setVisible(true);
                        px.update(line.split(Comm.DELIMITER)[3]);

                    } else {
                        System.out.println(line);
                        listModel.addElement(line);
                        //autoscroll: new message received, then scroll down
                        list.ensureIndexIsVisible(listModel.size()-1);
                    }
                }
            }
        }.start();

        userList.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                JList list = (JList)evt.getSource();
                if (evt.getClickCount() == 2) {
                    int idx = list.locationToIndex(evt.getPoint()); 
                    String recvNick = (String)list.getModel()
                        .getElementAt(idx);
                    //System.out.println("Clicked Nick is:" + recvNick +"\\");
                    //System.out.println("User Nick is:" + nick +"\\");
                    if (idx != 0 && !recvNick.equals(nick)) {
                        if (!xatList.containsKey(recvNick)) { 
                            createPrivateXat(recvNick);
                        } else {
                            xatList.get(recvNick).setVisible(true);
                        } 
                    }
                }
            }
        });

    }


    public void createPrivateXat(String recvNick) {
        JFrame frame = new JFrame(recvNick);
        frame.setDefaultCloseOperation(
                JFrame.DISPOSE_ON_CLOSE
                /*JFrame.DO_NOTHING_ON_CLOSE*/);

        PrivateXat pxat = new PrivateXat(ms, nick, recvNick);
        
        //Create and set up the content pane.
        JComponent newContentPane = pxat; 
        newContentPane.setOpaque(true);
        frame.setContentPane(newContentPane);
        // FOR CLOSING SMALL XAT
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                xatList.remove(recvNick);
                frame.dispose();
            }
        });


        //Display the window.
        //frame.pack();
        frame.setSize(new Dimension(265,314));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        xatList.put(recvNick,pxat);
    }

    public void actionPerformed(ActionEvent event){
        Object source = event.getSource();
        String text = entry.getText();
        entry.setText("");
        listModel.addElement("me:" + text);
        //autoscroll
        list.ensureIndexIsVisible(listModel.size()-1);
        ms.println(text);
    }



    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {

        //Set the look and feel.
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e){
            //nothing to do here
        }

        //Make sure we have nice window decorations.
        JFrame.setDefaultLookAndFeelDecorated(true);

        //Create and set up the window.
        JFrame  frame = new JFrame("Xat");
        frame.setDefaultCloseOperation(
                JFrame./*EXIT_ON_CLOSE*/DO_NOTHING_ON_CLOSE);

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                //Send disconnection
                ms.println(Comm.DISCONNECT);
                System.exit(0);
            }
        });

        //Create and set up the content pane.
        JComponent newContentPane = new Xat();
        newContentPane.setOpaque(true);
        frame.setContentPane(newContentPane);


        //Display the window.
        //frame.pack();
        frame.setSize(new Dimension(400,500));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.

        if (args.length > 2) {
            ip = args[0];
            nick = args[2];
            try {
                port = Integer.parseInt(args[1]);
                ms = new MySocket(ip, port);
            } catch (NumberFormatException ex) {
                System.out.println("Number Port Exception");
                return;
            }
        } else {
            System.out.println("Usage: java Xat 127.0.0.1 2222 Pepe");
            return;
        }

        //System.out.println("log 1");

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });

    }
}
