package src;

import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;

import src.MySocket;

public class MyServerSocket { 

    private ServerSocket ss;

    public MyServerSocket(int port) {
        try{
            this.ss = new ServerSocket(port);
        } catch(IOException ex) {
            ex.printStackTrace();
        } finally {
        }
    }

    public MySocket accept(){
        //add socket to container ( hash map with keys nick)
        try{
            return new MySocket(ss.accept());
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /*    public MySocket getSocket( String nick){
	search in hashmap for nick key
	return socket of nick
	}*/
}
