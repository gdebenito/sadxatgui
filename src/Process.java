/* File-Name:   Process.java
   Date:        2017 Apr 18
   Author:      Gonzalo De Benito
   Purpose:     Send and recieves information for each Client.
   Data Used:   None
   Output File: Process.class
   Data Output: Console output
*/

package src;

import java.io.*;
import java.lang.*;
import java.util.*;

class Process extends Thread {

    Monitor m;
    MySocket s;
    String nick;
    
    public Process(Monitor mon, MySocket ms){
	m = mon;
	s = ms;
    }

    @Override
    public void run() {
	String line;
	//retrieve nick
	s.println(Comm.NICK);
	nick = s.readLine();

	//add to hashmap
	while (m.hasNick(nick)) {
	    s.println("Nick choosed.\nGet new nick:");
	    nick = s.readLine();
	} 
        s.println(Comm.NEWNICK + nick);
        //add client to the HashMap
	m.add(nick,s);
        //notify all conected clients new user connected
        m.sendAllBut(nick, Comm.NEWUSER + nick);
        //send new user the clients nick
        m.userList(s);

	//connected mode, disconnect if 
	while((line = s.readLine()) != null){
	    if (line.matches(Comm.DISCONNECT)){
		s.println(Comm.DISCONNECT);
		m.sendAll(Comm.DELUSER + nick); 
		m.remove(nick);
		try {
		    s.close();
		} catch ( IOException ex ){
		    ex.printStackTrace();
		}
		break;
            } else if (line.contains(Comm.PRIVATEMSG)) {
                System.out.println("PRMESSAGE:" + line);
                m.send(line.split(Comm.DELIMITER)[2], line); 
	    } else {
		m.sendMssg(nick,line);
	    }
	}
    }
}
