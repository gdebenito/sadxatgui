package src;

import java.awt.*;
import java.awt.event.*;
import java.awt.event.WindowAdapter;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.SwingWorker;
import java.util.List;
import java.io.IOException;
import src.MySocket;
import src.Comm;

public class PrivateXat extends JPanel implements ActionListener{

    // define a JList and a DefaultListModel
    private JList<String> list;
    private static DefaultListModel<String> listModel;
    private JTextField entry;
    private JButton add;
    private JScrollPane scroll;

    private static String nick;
    private static String recvNick;
    private static MySocket ms;

    public PrivateXat(MySocket msocket, String nick, String recvNick) {
        super(new BorderLayout());
        ms = msocket; 
        this.nick = nick;
        this.recvNick = recvNick;

        // create initial listModel
        listModel = new DefaultListModel<>();
        listModel.addElement("Welcome to the chat");

        //Create the list and put it in a scroll pane.
        list = new JList<>(listModel);
        JScrollPane listScrollPane = new JScrollPane(list);
        add(listScrollPane, BorderLayout.CENTER);

        JPanel inp = new JPanel();
        inp.setLayout(new BoxLayout(inp, BoxLayout.LINE_AXIS));
        add = new JButton("Send");
        entry = new JTextField();

        inp.add(entry);
        inp.add(add);

        entry.addActionListener(this);
        add.addActionListener(this);

        add(inp, BorderLayout.PAGE_END);

    } 
    public void actionPerformed(ActionEvent event){
        Object source = event.getSource();
        String text = entry.getText();
        entry.setText("");
        listModel.addElement("me:" + text);
        text = Comm.PRIVATEMSG + Comm.DELIMITER  +  nick     + 
                                 Comm.DELIMITER  +  recvNick + 
                                 Comm.DELIMITER  +  text;
        //autoscroll
        list.ensureIndexIsVisible(listModel.size()-1);
        ms.println(text);
    }

    public void update(String msg) {
        listModel.addElement(recvNick +":"+ msg);
    }
}
